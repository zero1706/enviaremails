﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AE.Net.Mail;
using Microsoft.Exchange.WebServices.Data;

namespace LeerCorreo
{
    class Program
    {
        static void Main(string[] args)
        {
            /* ExchangeService exchange = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
             exchange.Credentials = new WebCredentials("sistemas09", "jlinoCK2020", "cottonknit.com");            
            // exchange.AutodiscoverUrl("jlino@cottonknit.com", (discoverURL) => true);           

             exchange.TraceEnabled = true;

             if (exchange != null)
             {
                 FindItemsResults<Item> result = exchange.FindItems(WellKnownFolderName.Inbox, new ItemView(100));
                 foreach (Item item in result)
                 {
                     EmailMessage message = EmailMessage.Bind(exchange, item.Id);
                     String body = message.Body.Text;
                     String from = message.From.Id.ToString();
                     String subject = message.Subject.ToString();
                 }
             }*/
            using (var client = new AE.Net.Mail.ImapClient(
             "mail.live.com",
             "myemail@outlook.com",
             "password_SV",
             ImapClient.AuthMethods.Login, 993, false
             ))
            {
                client.SelectMailbox("INBOX");
                AE.Net.Mail.MailMessage[] mailmessage = client.GetMessages(0, 5, false, true);

                foreach (AE.Net.Mail.MailMessage msg in mailmessage)
                {
                    collection.Add(new Message()
                    {
                        Id = msg.MessageID,
                        Sender = msg.Sender,
                        Subject = msg.Subject,
                        Content = msg.Body
                    });
                }
                client.Logout();
                client.Disconnect();

                list.ItemsSource = collection;

            }
    }
}
