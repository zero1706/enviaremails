﻿using System;
using Microsoft.Exchange.WebServices.Data;

namespace LeeCorreo
{
    class Program
    {
        private static bool discoverUrl;

        static void Main(string[] args)
        {
            ExchangeService exchange = new ExchangeService(ExchangeVersion.Exchange2010);
            exchange.Credentials = new WebCredentials("Username", "Password", "Domain");
            exchange.AutodiscoverUrl("jlino@cottonknit.com"/*,(discoverUrl)=>true*/);

            if (exchange != null)
            {
                FindItemsResults<Item> result = exchange.FindItems(WellKnownFolderName.Inbox, new ItemView(100));
                foreach (Item item in result)
                {
                    EmailMessage message = EmailMessage.Bind(exchange, item.Id);
                    String body = message.Body.Text;
                    String from = message.From.Id.ToString();
                    String subject = message.Subject.ToString();
                }
            }
        }
    }
}
