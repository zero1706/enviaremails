﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadEmail
{
    public class OutlookEmail
    {

        public string EmailSender { get; set; }
        public string EmailFrom { get; set; }
        public string EmailFrom2 { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }

        public DateTime EmailDate { get; set; }
        public static List<OutlookEmail> ReadMailItems()
        {
            Application outloockApplication = null;
            NameSpace outlookNamespace = null;
            MAPIFolder inboxFolder = null;

            Items mailItems = null;

            List<OutlookEmail> listEmailDetails = new List<OutlookEmail>();
            OutlookEmail emailDetails;

            try
            {
                outloockApplication = new Application();
                outlookNamespace = outloockApplication.GetNamespace("MAPI");
                inboxFolder = outlookNamespace.GetDefaultFolder(OlDefaultFolders.olFolderInbox);

                
                mailItems = inboxFolder.Items;
                foreach (MailItem item in mailItems)
                {
                        if (mailItems.Count == 10) break;
                        emailDetails = new OutlookEmail();
                        emailDetails.EmailDate = item.ReceivedTime;
                        emailDetails.EmailSender = item.SenderName;
                        emailDetails.EmailFrom2 = item.To;
                        emailDetails.EmailFrom = item.SenderEmailType;
                        emailDetails.EmailSubject = item.Subject;
                        emailDetails.EmailBody = item.Body;

                        listEmailDetails.Add(emailDetails);
                        ReleaseComObject(item);
                    
                }
            }
            catch (System.Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally 
            {
                ReleaseComObject(mailItems);
                ReleaseComObject(inboxFolder);
                ReleaseComObject(outlookNamespace);
                ReleaseComObject(outloockApplication);
            }
            return listEmailDetails;
        }
        private static void ReleaseComObject(object obj)
        {
            if (obj !=null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
        }
    }
}
