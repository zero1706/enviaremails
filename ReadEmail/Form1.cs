﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.IO;
using System.Reflection;
using Alertas;
using System.Diagnostics;

namespace ReadEmail
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            buscar();
            
            
        }
        DataTable dt;

        private void buscar()
        {
            try
            {
                Outlook._Application _app = new Outlook.Application();
                Outlook._NameSpace _ns = _app.GetNamespace("MAPI");
                Outlook.MAPIFolder Inbox = _ns.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);
                _ns.SendAndReceive(true);
                dt = new DataTable("Inbox");
                dt.Columns.Add("Subject", typeof(string));
                dt.Columns.Add("Sender", typeof(string));
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("Body", typeof(string));
                dt.Columns.Add("ADJ", typeof(string));

                foreach (Outlook.MailItem item in Inbox.Items)
                {
                    dt.Rows.Add(new object[] { item.Subject, item.SenderName, item.SentOn.ToShortDateString(), item.HTMLBody, item.Attachments.Count });
                }
                DataView dv = new DataView(dt);
                dv.RowFilter = "(DATE>= '" + dtpInicio.Value.ToShortDateString() + "' AND DATE <= '" + dtpFin.Value.ToShortDateString() + "')";
                dataGridView1.DataSource = dv;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(dt);
            dv.RowFilter = "(DATE>= '" + dtpInicio.Value.ToShortDateString() + "' AND DATE <= '" + dtpFin.Value.ToShortDateString() + "')";
            if (DateTime.Parse(dtpInicio.Value.ToShortDateString()) >= DateTime.Parse(dtpFin.Value.ToShortDateString()))
            {
                MessageBox.Show("Orden de fechas incorrecto", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else { dataGridView1.DataSource = dv; }

        }


        private void btnActualizar_Click(object sender, EventArgs e)
        {
            buscar();
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            Outlook.Application Application = new Outlook.Application();
            Outlook.MailItem ForwardingMessage = (Outlook.MailItem)Application.CreateItem(Outlook.OlItemType.olMailItem);
            Outlook.MAPIFolder templateFolder = Application.Session.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderInbox).Folders["Prueba"];
            Outlook.MAPIFolder destfolder = Application.Session.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderInbox).Folders["Enviados"];
            //const string PR_SMTP_ADDRESS = "http://schemas.microsoft.com/mapi/proptag/0x39FE001E";


            //Cambiar el SenderEmailAddress por el correo de la persona a filtrar mensajes, en ese mismo formato
            foreach (Outlook.MailItem item in templateFolder.Items)
            // foreach (Outlook.MailItem item in templateFolder.Items.Restrict(" [ReceivedTime] >= 'DateTime.Now.AddDays(-1).ToString()'  And  [SenderEmailAddress] = '/O=COTTONKNIT/OU=EXCHANGE ADMINISTRATIVE GROUP (FYDIBOHF23SPDLT)/CN=RECIPIENTS/CN=GERARDO ORELLANA' "))           
            {

                ForwardingMessage = (Outlook.MailItem)templateFolder.Items.Add("IPM.Note.OutlookCustomForm1");


                if (item != null)
                {
                    if (ForwardingMessage != null)
                    {
                        EnviaCorreo enviar = new EnviaCorreo();

                        ForwardingMessage.Subject = "FW: " + item.Subject;
                        #region Attahment 


                        int attachCount = item.Attachments.Count;
                        if (attachCount > 0)
                        {

                            for (int idx = 1; idx <= attachCount; idx++)
                            {
                                string sysPath = System.IO.Path.GetTempPath();
                                if (!System.IO.Directory.Exists(sysPath + "~test"))
                                {
                                    System.IO.Directory.CreateDirectory(sysPath + "~test");
                                }
                                //obtener el archivo adjunto y guardarlo en la carpeta temporal


                                string strSourceFileName = sysPath + "~test\\" + item.Attachments[idx].FileName;
                                item.Attachments[idx].SaveAsFile(strSourceFileName);
                                string strDisplayName = "Attachment";
                                int intPosition = 1;
                                int intAttachType = (int)Outlook.OlAttachmentType.olEmbeddeditem;

                                ForwardingMessage.Attachments.Add(strSourceFileName, intAttachType, intPosition, strDisplayName);
                            }
                        }
                        #endregion
                        #region Body 
                        string Header = "<p><br><br>" +
                                "-----Message Imformation-----" + "<br>";
                        Header += "From: " + Application.Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress + "<br>";
                        Header += "Sent: " + item.SentOn.ToString() + "<br>";
                        Header += "To: " + "jcaramantin@cottonknit.com" + " <br>";
                        Header += "Subject: " + item.Subject + "<br><br>";

                        ForwardingMessage.HTMLBody = Header + GetTrimmedBodyText(item.HTMLBody);
                        ForwardingMessage.To = "jcaramantin@cottonknit.com";

                        #endregion
                        ForwardingMessage.Display(false);
                        //ForwardingMessage.Send();
                        //item.Move(destfolder);
                    }

                }
                ForwardingMessage = null;

            }

            string GetTrimmedBodyText(string strHTMLBody)
            {
                string strTrimmedHTMLBody = strHTMLBody;
                int start = 0;
                start = strHTMLBody.IndexOf("<img", start);

                while (start > 0)
                {

                    int count = strHTMLBody.IndexOf('>', start);
                    string str = strHTMLBody.Substring(start);
                    string strActualImgTag = str.Substring(0, str.IndexOf(">") + 1);
                    string strTrimImgTag = strActualImgTag.Replace("cid:", "");
                    int intAtPosition = 0;
                    intAtPosition = strTrimImgTag.IndexOf("@");
                    while (intAtPosition > 0)
                    {
                        string strAt =
                        strTrimImgTag.Substring(strTrimImgTag.IndexOf("@"), 18);
                        strTrimImgTag = strTrimImgTag.Replace(strAt, "");
                        intAtPosition = strTrimImgTag.IndexOf("@");
                    }
                    strTrimmedHTMLBody = strTrimmedHTMLBody.Replace(strActualImgTag, strTrimImgTag);
                    start = strHTMLBody.IndexOf("<img", start + 1);
                }
                return strTrimmedHTMLBody;
            }

        }

    }
}
