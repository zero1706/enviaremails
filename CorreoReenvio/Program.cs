﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection; // to use Missing.Value
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace CorreoReenvio
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Outlook.Application Application = new Outlook.Application();
                //Outlook.NameSpace oNS = Application.GetNamespace("mapi");
                //oNS.Logon(Missing.Value, Missing.Value, false, true);
                Outlook.MailItem ForwardingMessage = (Outlook.MailItem)Application.CreateItem(Outlook.OlItemType.olMailItem);
                Outlook.MAPIFolder templateFolder = Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox).Folders["CONTROL_FE"];
                Outlook.MAPIFolder destfolder = Application.Session.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderInbox).Folders["FACT_ELECTRONICAS"];
                //const string PR_SMTP_ADDRESS = "http://schemas.microsoft.com/mapi/proptag/0x39FE001E";


                //Cambiar el SenderEmailAddress por el correo de la persona a filtrar mensajes, en ese mismo formato
                foreach (Outlook.MailItem item in templateFolder.Items)
                // foreach (Outlook.MailItem item in templateFolder.Items.Restrict(" [ReceivedTime] >= 'DateTime.Now.AddDays(-1).ToString()'  And  [SenderEmailAddress] = '/O=COTTONKNIT/OU=EXCHANGE ADMINISTRATIVE GROUP (FYDIBOHF23SPDLT)/CN=RECIPIENTS/CN=GERARDO ORELLANA' "))           
                {

                    ForwardingMessage = (Outlook.MailItem)templateFolder.Items.Add("IPM.Note.OutlookCustomForm1");


                    if (item != null)
                    {
                        if (ForwardingMessage != null)
                        {

                            ForwardingMessage.Subject = "FW: " + item.Subject;
                            #region Attahment 


                            int attachCount = item.Attachments.Count;
                            if (attachCount > 0)
                            {

                                for (int idx = 1; idx <= attachCount; idx++)
                                {
                                    string sysPath = System.IO.Path.GetTempPath();
                                    if (!System.IO.Directory.Exists(sysPath + "~test"))
                                    {
                                        System.IO.Directory.CreateDirectory(sysPath + "~test");
                                    }
                                    //obtener el archivo adjunto y guardarlo en la carpeta temporal
                                    string [] extensionsArray = { ".pdf" };
                                    string[] extensionsArray1 = { ".xml",".XML" };
                                    if (extensionsArray1.Any(item.Attachments[idx].FileName.Contains))
                                    {
                                        string narhivo = item.Attachments[idx].DisplayName.Substring(0, 1);
                                        if (item.Attachments[idx].DisplayName.Substring(0, 2) == "R-")
                                        {
                                        }
                                        else
                                        {
                                            string strSourceFileName = sysPath + "~test\\" + item.Attachments[idx].FileName;
                                            item.Attachments[idx].SaveAsFile(strSourceFileName);
                                            string strDisplayName = "Attachment";
                                            int intPosition = 1;
                                            int intAttachType = (int)Outlook.OlAttachmentType.olEmbeddeditem;

                                            ForwardingMessage.Attachments.Add(strSourceFileName, intAttachType, intPosition, strDisplayName);
                                        }
                                    }
                                    if (extensionsArray.Any(item.Attachments[idx].FileName.Contains))
                                    {
                                        
                                            string strSourceFileName = sysPath + "~test\\" + item.Attachments[idx].FileName;
                                            item.Attachments[idx].SaveAsFile(strSourceFileName);
                                            string strDisplayName = "Attachment";
                                            int intPosition = 1;
                                            int intAttachType = (int)Outlook.OlAttachmentType.olEmbeddeditem;

                                            ForwardingMessage.Attachments.Add(strSourceFileName, intAttachType, intPosition, strDisplayName);
                                        
                                    }

                                    
                                }
                            }
                            #endregion
                            #region Body 
                            string Header = "<p><br><br>" +
                                    "-----Información del mensaje-----" + "<br>";
                            Header += "From: " + Application.Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress + "<br>";
                            Header += "Sent: " + item.SentOn.ToString() + "<br>";
                            Header += "To: " + "jcaramantin@cottonknit.com" + " <br>";
                            Header += "Subject: " + item.Subject + "<br><br>";

                            ForwardingMessage.HTMLBody = Header + GetTrimmedBodyText(item.HTMLBody);
                            ForwardingMessage.To = "jcaramantin@cottonknit.com";

                            #endregion
                            //ForwardingMessage.Display(false);
                            ForwardingMessage.Send();
                            item.Move(destfolder);
                        }

                    }
                    ForwardingMessage = null;

                }

                string GetTrimmedBodyText(string strHTMLBody)
                {
                    string strTrimmedHTMLBody = strHTMLBody;
                    int start = 0;
                    start = strHTMLBody.IndexOf("<img", start);

                    while (start > 0)
                    {

                        int count = strHTMLBody.IndexOf('>', start);
                        string str = strHTMLBody.Substring(start);
                        string strActualImgTag = str.Substring(0, str.IndexOf(">") + 1);
                        string strTrimImgTag = strActualImgTag.Replace("cid:", "");
                        int intAtPosition = 0;
                        intAtPosition = strTrimImgTag.IndexOf("@");
                        while (intAtPosition > 0)
                        {
                            string strAt =
                            strTrimImgTag.Substring(strTrimImgTag.IndexOf("@"), 18);
                            strTrimImgTag = strTrimImgTag.Replace(strAt, "");
                            intAtPosition = strTrimImgTag.IndexOf("@");
                        }
                        strTrimmedHTMLBody = strTrimmedHTMLBody.Replace(strActualImgTag, strTrimImgTag);
                        start = strHTMLBody.IndexOf("<img", start + 1);
                    }
                    return strTrimmedHTMLBody;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
        }
    }
}

